<?php

namespace JWT;

use JWT\Parts\Payload;

/**
 * Class Token
 * @package JWT
 */
class Token
{
    /**
     * Additional claims
     */
    private const CLAIM_USER_ID = 'user_id';

    /**
     * @var array
     */
    private const LC_CLAIMS_INTO_TOKEN = [
        'aud' => 'audience',
        'jti' => 'uniqueId',
        'iat' => 'issuedAt',
        'nbf' => 'notBefore',
        'exp' => 'expiration',
        'sub' => 'subject',
        'iss' => 'issuer',
    ];

    /**
     * @var Payload
     */
    private $payload;

    /**
     * Token constructor.
     */
    public function __construct()
    {
        $this->payload = new Payload();
    }

    /**
     * @param string $userId
     * @return Token
     */
    public function setUserId(string $userId): Token
    {
        $this->getPayload()->set(self::CLAIM_USER_ID, $userId);

        return $this;
    }

    /**
     * @return string
     * @throws exceptions\NotExistingClaimException
     */
    public function getUserId(): string
    {
        return $this->getPayload()->get(self::CLAIM_USER_ID);
    }

    /**
     * @return Payload
     */
    public function getPayload(): Payload
    {
        return $this->payload;
    }

    /**
     * @param string $issuer
     * @return Token
     */
    public function setIssuer(string $issuer): Token
    {
        $this->getPayload()->setIssuer($issuer);

        return $this;
    }

    /**
     * @param string $subject
     * @return Token
     */
    public function setSubject(string $subject): Token
    {
        $this->getPayload()->setSubject($subject);

        return $this;
    }

    /**
     * @param string $audience
     * @return Token
     */
    public function setAudience(string $audience): Token
    {
        $this->getPayload()->setAudience($audience);

        return $this;
    }

    /**
     * @param string $expiration
     * @return Token
     */
    public function setExpiration(string $expiration): Token
    {
        $this->getPayload()->setExpiration($expiration);

        return $this;
    }

    /**
     * @param string $notBefore
     * @return Token
     */
    public function setNotBefore(string $notBefore): Token
    {
        $this->getPayload()->setNotBefore($notBefore);

        return $this;
    }

    /**
     * @param string $issuedAt
     * @return Token
     */
    public function setIssuedAt(string $issuedAt): Token
    {
        $this->getPayload()->setIssuedAt($issuedAt);

        return $this;
    }

    /**
     * @param string $uniqueId
     * @return Token
     */
    public function setUniqueId(string $uniqueId): Token
    {
        $this->getPayload()->setUniqueId($uniqueId);

        return $this;
    }

    /**
     * @param \Lcobucci\JWT\Token $token
     * @return $this
     */
    public function importFromLcobucciToken(\Lcobucci\JWT\Token $token)
    {
        foreach (self::LC_CLAIMS_INTO_TOKEN as $lcClaim => $tokenClaim) {
            $claimValue = $token->getClaim($lcClaim);
            if ($claimValue !== null) {
                $this->{'set' . ucfirst($tokenClaim)}($claimValue);
            }
        }
        return $this;
    }
}
