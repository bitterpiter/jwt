<?php

namespace JWT\Parts;

use JWT\exceptions\NotExistingClaimException;
use Lcobucci\JWT\Builder;

/**
 * JWT Token Payload
 * @package App\Library\JWT\Token
 */
class Payload
{
    /**
     * @var string
     */
    private $issuer;

    /**
     * @var string
     */
    private $subject;

    /**
     * @var string
     */
    private $audience;

    /**
     * @var string
     */
    private $expiration;

    /**
     * @var string
     */
    private $notBefore;

    /**
     * @var string
     */
    private $issuedAt;

    /**
     * @var string
     */
    private $uniqueId;

    /**
     * @var array
     */
    private $data = [];

    /**
     * @return string
     */
    public function getIssuer(): ?string
    {
        return $this->issuer;
    }

    /**
     * @param string $issuer
     *
     * @return Payload
     */
    public function setIssuer(string $issuer): Payload
    {
        $this->issuer = $issuer;

        return $this;
    }

    /**
     * @return string
     */
    public function getSubject(): ?string
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     *
     * @return Payload
     */
    public function setSubject(string $subject): Payload
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * @return string
     */
    public function getAudience(): ?string
    {
        return $this->audience;
    }

    /**
     * @param string $audience
     *
     * @return Payload
     */
    public function setAudience(string $audience): Payload
    {
        $this->audience = $audience;

        return $this;
    }

    /**
     * @return string
     */
    public function getExpiration(): ?string
    {
        return $this->expiration;
    }

    /**
     * @param string $expiration
     *
     * @return Payload
     */
    public function setExpiration(string $expiration): Payload
    {
        $this->expiration = $expiration;

        return $this;
    }

    /**
     * @return string
     */
    public function getNotBefore(): ?string
    {
        return $this->notBefore;
    }

    /**
     * @param string $notBefore
     *
     * @return Payload
     */
    public function setNotBefore(string $notBefore): Payload
    {
        $this->notBefore = $notBefore;

        return $this;
    }

    /**
     * @return string
     */
    public function getIssuedAt(): ?string
    {
        return $this->issuedAt;
    }

    /**
     * @param string $issuedAt
     *
     * @return Payload
     */
    public function setIssuedAt(string $issuedAt): Payload
    {
        $this->issuedAt = $issuedAt;

        return $this;
    }

    /**
     * @return string
     */
    public function getUniqueId(): ?string
    {
        return $this->uniqueId;
    }

    /**
     * @param string $uniqueId
     *
     * @return Payload
     */
    public function setUniqueId(string $uniqueId): Payload
    {
        $this->uniqueId = $uniqueId;

        return $this;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param string $key
     * @param string|array $value
     *
     * @return Payload
     */
    public function set(string $key, $value): Payload
    {
        $this->data[$key] = $value;

        return $this;
    }

    /**
     * @param string $key
     * @return string
     * @throws NotExistingClaimException
     */
    public function get(string $key): string
    {
        if (!array_key_exists($key, $this->data)) {
            throw new NotExistingClaimException($key);
        }

        return $this->data[$key];
    }
}
