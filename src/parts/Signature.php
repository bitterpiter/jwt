<?php

namespace JWT\Parts;

use JWT\config\AbstractConfig;
use Lcobucci\JWT\Signer;
use Lcobucci\JWT\Signer\Key;

/**
 * Class Signature
 * @package JWT\Parts
 */
class Signature
{
    /**
     * @var Signer
     */
    private $signer;

    /**
     * @var Key
     */
    private $key;

    /**
     * @var string
     */
    private $passphrase;

    /**
     * Signature constructor.
     * @param AbstractConfig $config
     */
    public function __construct(AbstractConfig $config)
    {
        $this->setSigner($config->getSigner());
        $this->setKey($config->getKey());
        $this->setPassphrase($config->getPassphrase());
    }

    /**
     * @return string
     */
    public function getPassphrase(): string
    {
        return $this->passphrase;
    }

    /**
     * @param string|null $passphrase
     * @return Signature
     */
    public function setPassphrase(string $passphrase = null): Signature
    {
        $this->passphrase = $passphrase;

        return $this;
    }

    /**
     * @return Signer
     */
    public function getSigner(): Signer
    {
        return $this->signer;
    }

    /**
     * @param Signer $signer
     * @return Signature
     */
    public function setSigner(Signer $signer): Signature
    {
        $this->signer = $signer;

        return $this;
    }

    /**
     * @return Key
     */
    public function getKey(): Key
    {
        return $this->key;
    }

    /**
     * @param Key $key
     * @return Signature
     */
    public function setKey(Key $key): Signature
    {
        $this->key = $key;

        return $this;
    }
}
