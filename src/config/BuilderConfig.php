<?php

namespace JWT\config;

use JWT\exceptions\InvalidConfigException;

/**
 * Class BuilderConfig
 * @package JWT\config
 */
class BuilderConfig extends AbstractConfig
{
    public const CONFIG_PRIVATE_KEY_PATH = 'private_key_path';

    /**
     * BuilderConfig constructor.
     * @param array $config
     * @throws InvalidConfigException
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);

        if (array_key_exists(self::CONFIG_PRIVATE_KEY_PATH, $config)) {
            $this->setKey($config[self::CONFIG_PRIVATE_KEY_PATH]);
        }
    }
}
