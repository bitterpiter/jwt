<?php

namespace JWT\config;

use JWT\exceptions\InvalidConfigException;
use JWT\Parts\Signature;
use JWT\Signer\AlgorithmFactory;
use JWT\Signer\AlgorithmInterface;
use Lcobucci\JWT\Signer;
use Lcobucci\JWT\Signer\Key;

/**
 * Class AbstractConfig
 * @package JWT\config
 */
abstract class AbstractConfig
{
    private const CONFIG_PASSPHRASE = 'passphrase';
    private const CONFIG_SIGN_ALGORITHM = 'sign_algorithm';

    private const DEFAULT_SIGN_ALGORITHM = AlgorithmInterface::HASH_RS_256;

    /**
     * @var string
     */
    protected $passphrase;

    /**
     * @var Signer
     */
    protected $signer;

    /**
     * @var Key
     */
    protected $key;

    /**
     * @param array $config
     * @throws InvalidConfigException
     */
    public function __construct(array $config = [])
    {
        if (array_key_exists(self::CONFIG_PASSPHRASE, $config)) {
            $this->setPassphrase($config[self::CONFIG_PASSPHRASE]);
        }

        if (array_key_exists(self::CONFIG_SIGN_ALGORITHM, $config)) {
            $this->setSigner($config[self::CONFIG_SIGN_ALGORITHM]);
        } else {
            $this->setSigner(self::DEFAULT_SIGN_ALGORITHM);
        }
    }

    /**
     * @return Key
     */
    public function getKey(): Key
    {
        return $this->key;
    }

    /**
     * @return string|null
     */
    public function getPassphrase(): ?string
    {
        return $this->passphrase;
    }

    /**
     * @return Signer
     */
    public function getSigner(): Signer
    {
        return $this->signer;
    }

    /**
     * @param string $keyPath
     * @return AbstractConfig
     * @throws InvalidConfigException
     */
    protected function setKey(string $keyPath): AbstractConfig
    {
        if (!is_file($keyPath)) {
            throw new InvalidConfigException('Wrong key path: ' . $keyPath);
        }

        try {
            $this->key = new Key("file://{$keyPath}", $this->passphrase);
        } catch (\Exception $e) {
            throw new InvalidConfigException('Invalid key', $e->getCode(), $e);
        }

        return $this;
    }

    /**
     * @param string $passphrase
     * @return AbstractConfig
     */
    private function setPassphrase(string $passphrase): AbstractConfig
    {
        $this->passphrase = $passphrase;

        return $this;
    }

    /**
     * @param string $signAlgorithm
     * @return AbstractConfig
     * @throws InvalidConfigException
     */
    private function setSigner(string $signAlgorithm): AbstractConfig
    {
        try {
            $this->signer = AlgorithmFactory::create($signAlgorithm);
        } catch (\Exception $e) {
            throw new InvalidConfigException('Invalid signing algorithm: ' . $signAlgorithm, $e->getCode(), $e);
        }

        return $this;
    }

    /**
     * @return Signature
     */
    public function getSignature(): Signature
    {
        return new Signature($this);
    }
}
