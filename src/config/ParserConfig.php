<?php

namespace JWT\config;

use JWT\exceptions\InvalidConfigException;

/**
 * Class ParserConfig
 * @package JWT\config
 */
class ParserConfig extends AbstractConfig
{
    public const CONFIG_PUBLIC_KEY_PATH = 'public_key_path';

    /**
     * ParserConfig constructor.
     * @param array $config
     * @throws InvalidConfigException
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);

        if (array_key_exists(self::CONFIG_PUBLIC_KEY_PATH, $config)) {
            $this->setKey($config[self::CONFIG_PUBLIC_KEY_PATH]);
        }
    }
}
