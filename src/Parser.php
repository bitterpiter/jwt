<?php

namespace JWT;

use JWT\config\AbstractConfig;
use JWT\exceptions\ParserException;
use JWT\Parts\Signature;

/**
 * Class Parser
 * @package JWT
 */
class Parser
{
    /**
     * @var string
     */
    private $jwtString;

    /**
     * @var Signature
     */
    private $signature;

    /**
     * Parser constructor.
     * @param string $jwtString
     * @param AbstractConfig $config
     * @throws ParserException
     */
    public function __construct(string $jwtString, AbstractConfig $config)
    {
        if (empty($jwtString)) {
            throw new ParserException('JWT string token is required.');
        }

        $this->jwtString = $jwtString;
        $this->signature = $config->getSignature();
    }

    /**
     * @return Token
     * @throws \OutOfBoundsException
     * @throws ParserException
     */
    public function parse(): Token
    {
        $lcToken = $this->parseTokenString();
        $this->verifyTokenSignature($lcToken);
        $this->verifyTokenExpiration($lcToken);

        return (new Token())->importFromLcobucciToken($lcToken);
    }

    /**
     * @return \Lcobucci\JWT\Token
     * @throws ParserException
     */
    private function parseTokenString(): \Lcobucci\JWT\Token
    {
        try {
            return (new \Lcobucci\JWT\Parser())->parse($this->jwtString);
        } catch (\Exception $exception) {
            throw new ParserException('Cannot parse token.', 0, $exception);
        }
    }

    /**
     * @param \Lcobucci\JWT\Token $token
     * @throws ParserException
     */
    private function verifyTokenSignature(\Lcobucci\JWT\Token $token): void
    {
        try {
            if (!$token->verify($this->signature->getSigner(), $this->signature->getKey())) {
                throw new ParserException('Signature could not be verified');
            }
        } catch (\BadMethodCallException $e) {
            throw new ParserException('Token is not signed');
        }
    }

    /**
     * @param \Lcobucci\JWT\Token $token
     * @throws ParserException
     */
    private function verifyTokenExpiration(\Lcobucci\JWT\Token $token): void
    {
        if ($token->isExpired()) {
            throw new ParserException('Token expired');
        }
    }
}
