<?php

namespace JWT;

use JWT\config\AbstractConfig;
use JWT\Parts\Payload;
use JWT\Parts\Signature;
use Lcobucci\JWT\Builder as JWTBuilder;
use Lcobucci\JWT\Token as JWTToken;

/**
 * Class Builder
 */
class Builder
{
    private const REGISTERED_CLAIMS = [
        'audience' => 'audience',
        'id' => 'uniqueId',
        'issuedAt' => 'issuedAt',
        'notBefore' => 'notBefore',
        'expiration' => 'expiration',
        'subject' => 'subject',
        'issuer' => 'issuer',
    ];

    /**
     * @var Token
     */
    private $token;

    /**
     * @var JWTToken
     */
    private $jwtToken;

    /**
     * @var Signature
     */
    private $signature;

    /**
     * Builder constructor.
     * @param AbstractConfig $config
     * @param Token|null $token
     */
    public function __construct(AbstractConfig $config, Token $token = null)
    {
        $this->token = $token ?: new Token();
        $this->signature = $config->getSignature();
    }

    /**
     * @throws \BadMethodCallException
     */
    public function build(): string
    {
        $jwtBuilder = new JWTBuilder();
        $this->setPayloadData($jwtBuilder);

        if ($this->signature instanceof Signature) {
            $this->signBuilder($jwtBuilder);
        }

        $this->jwtToken = $jwtBuilder->getToken();

        return $this->jwtToken;
    }

    /**
     * @param JWTBuilder $jwtBuilder
     * @throws \BadMethodCallException
     */
    private function setPayloadData(JWTBuilder $jwtBuilder): void
    {
        $payload = $this->token->getPayload();
        $this->setRegisteredClaims($jwtBuilder, $payload);
        $this->setAdditionalClaims($jwtBuilder, $payload);
    }

    /**
     * @param JWTBuilder $jwtBuilder
     * @param Payload $payload
     */
    private function setRegisteredClaims(JWTBuilder $jwtBuilder, Payload $payload): void
    {
        foreach (self::REGISTERED_CLAIMS as $builderMethod => $payLoadMethod) {
            $claimValue = $payload->{'get' . ucfirst($payLoadMethod)}();
            if ($claimValue !== null) {
                $jwtBuilder->{'set' . ucfirst($builderMethod)}($claimValue);
            }
        }
    }

    /**
     * @param JWTBuilder $jwtBuilder
     * @param Payload $payload
     * @throws \BadMethodCallException
     */
    private function setAdditionalClaims(JWTBuilder $jwtBuilder, Payload $payload): void
    {
        $data = $payload->getData();

        if (empty($data)) {
            return;
        }

        foreach ($data as $key => $value) {
            $jwtBuilder->set($key, $value);
        }
    }

    /**
     * @param JWTBuilder $jwtBuilder
     */
    private function signBuilder(JWTBuilder $jwtBuilder): void
    {
        $jwtBuilder->sign($this->signature->getSigner(), $this->signature->getKey());
    }
}
