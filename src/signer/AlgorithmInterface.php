<?php

namespace JWT\Signer;

/**
 * Interface SignerAlgorithmInterface
 * @package App\Library\JWT\Signer
 */
interface AlgorithmInterface
{
    public const HASH_ES_256 = 'ES256';
    public const HASH_ES_384 = 'ES384';
    public const HASH_ES_512 = 'ES512';

    public const HASH_HS_256 = 'HS256';
    public const HASH_HS_384 = 'HS384';
    public const HASH_HS_512 = 'HS512';

    public const HASH_RS_256 = 'RS256';
    public const HASH_RS_384 = 'RS384';
    public const HASH_RS_512 = 'RS512';
}
