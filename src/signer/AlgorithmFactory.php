<?php

namespace JWT\Signer;

use JWT\Exceptions\InvalidAlgorithmType;
use Lcobucci\JWT\Signer;

/**
 * Class SignerAlgorithmFactory
 * @package App\Library\JWT\Signer
 */
class AlgorithmFactory implements AlgorithmInterface
{
    /**
     * @param string $type
     * @return Signer
     * @throws InvalidAlgorithmType
     */
    public static function create($type = self::HASH_RS_256): Signer
    {
        switch ($type) {
            case self::HASH_ES_256:
                return new Signer\Ecdsa\Sha256();
            case self::HASH_ES_384:
                return new Signer\Ecdsa\Sha384();
            case self::HASH_ES_512:
                return new Signer\Ecdsa\Sha512();
            case self::HASH_HS_256:
                return new Signer\Hmac\Sha256();
            case self::HASH_HS_384:
                return new Signer\Hmac\Sha384();
            case self::HASH_HS_512:
                return new Signer\Hmac\Sha512();
            case self::HASH_RS_256:
                return new Signer\Rsa\Sha256();
            case self::HASH_RS_384:
                return new Signer\Rsa\Sha384();
            case self::HASH_RS_512:
                return new Signer\Rsa\Sha512();
            default:
                throw new InvalidAlgorithmType('Unknown JWT hashing algorithm: ' . $type);
        }
    }
}
