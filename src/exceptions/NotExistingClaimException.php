<?php

namespace JWT\exceptions;

/**
 * Class NotExistingClaimException
 * @package JWT\exceptions
 */
class NotExistingClaimException extends AbstractJwtException
{
    /**
     * NotExistingClaimException constructor.
     * @param string $key
     */
    public function __construct(string $key)
    {
        parent::__construct("Field '{$key}' does not exists in JWT payload.");
    }

}
