<?php
/**
 * Created by PhpStorm.
 * User: Michal
 * Date: 2018-04-23
 * Time: 20:33
 */

namespace JWT\exceptions;

/**
 * Class ParserException
 * @package JWT\exceptions
 */
class ParserException extends AbstractJwtException
{

}