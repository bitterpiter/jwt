<?php

namespace JWT\exceptions;

/**
 * Class InvalidConfigException
 * @package JWT\exceptions
 */
class InvalidConfigException extends AbstractJwtException
{

}
