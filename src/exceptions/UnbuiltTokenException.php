<?php

namespace JWT\exceptions;

/**
 * Class UnbuiltTokenException
 */
class UnbuiltTokenException extends \JWT\Exceptions\AbstractJwtException
{
    /**
     * UnbuiltTokenException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = '', $code = 0, Throwable $previous = null)
    {
        if (empty($message)) {
            $message = 'Token is not built yet.';
        }
        parent::__construct($message, $code, $previous);
    }
}
