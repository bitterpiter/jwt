<?php

namespace Tests\Unit;

use JWT\Builder;
use JWT\config\BuilderConfig;
use JWT\config\ParserConfig;
use JWT\Parser;
use JWT\Parts\Signature;
use JWT\Token;

/**
 * Class TokenTest
 * @package Tests\Unit
 */
class JwtTest extends AbstractTestCase
{
    /**
     * @throws \JWT\exceptions\InvalidConfigException
     */
    public function testBuildTokenSuccess(): void
    {
        $config = new BuilderConfig([BuilderConfig::CONFIG_PRIVATE_KEY_PATH => __DIR__ . '/keys/jwt-test.priv']);
        $builtToken = new Builder($config);
        $stringToken = $builtToken->build();

        $this->assertNotEmpty($stringToken);
    }

    /**
     * @throws \JWT\exceptions\NotExistingClaimException
     */
    public function testToken(): void
    {
        $issuer = $this->faker->text(50);
        $subject = $this->faker->text(50);
        $audience = $this->faker->text(50);
        $expiration = $this->faker->dateTimeBetween('now', '+1 year')->format('Y-m-d H:i:s');
        $notBefore = $this->faker->dateTime()->format('Y-m-d H:i:s');
        $issuedAt = $this->faker->date();
        $uniqueId = $this->faker->numberBetween(1, PHP_INT_MAX);
        $userId = $this->faker->numberBetween(1, 100);

        $token = new Token();
        $token->setIssuer($issuer);
        $token->setSubject($subject);
        $token->setAudience($audience);
        $token->setExpiration($expiration);
        $token->setNotBefore($notBefore);
        $token->setIssuedAt($issuedAt);
        $token->setUniqueId($uniqueId);
        $token->setUserId($userId);

        $this->assertEquals($issuer, $token->getPayload()->getIssuer());
        $this->assertEquals($subject, $token->getPayload()->getSubject());
        $this->assertEquals($audience, $token->getPayload()->getAudience());
        $this->assertEquals($expiration, $token->getPayload()->getExpiration());
        $this->assertEquals($notBefore, $token->getPayload()->getNotBefore());
        $this->assertEquals($issuedAt, $token->getPayload()->getIssuedAt());
        $this->assertEquals($uniqueId, $token->getPayload()->getUniqueId());
        $this->assertEquals($userId, $token->getUserId());
    }

    /**
     * @throws \JWT\exceptions\InvalidConfigException
     * @throws \JWT\exceptions\ParserException
     */
    public function testParser(): void
    {
        $issuer = $this->faker->text(50);
        $subject = $this->faker->text(50);
        $audience = $this->faker->text(50);
        $expiration = time() + 60;
        $notBefore = time();
        $issuedAt = time();
        $uniqueId = $this->faker->numberBetween(1, PHP_INT_MAX);

        $token = new Token();
        $token->setIssuer($issuer);
        $token->setSubject($subject);
        $token->setAudience($audience);
        $token->setExpiration($expiration);
        $token->setNotBefore($notBefore);
        $token->setIssuedAt($issuedAt);
        $token->setUniqueId($uniqueId);

        $builderConfig = new BuilderConfig([BuilderConfig::CONFIG_PRIVATE_KEY_PATH => __DIR__ . '/keys/jwt-test.priv']);
        $builtToken = new Builder($builderConfig, $token);
        $stringToken = $builtToken->build();

        $parserConfig = new ParserConfig([ParserConfig::CONFIG_PUBLIC_KEY_PATH => __DIR__ . '/keys/jwt-test.pub']);
        $parser = new Parser($stringToken, $parserConfig);
        $parserToken = $parser->parse();

        $this->assertEquals($issuer, $parserToken->getPayload()->getIssuer());
        $this->assertEquals($subject, $parserToken->getPayload()->getSubject());
        $this->assertEquals($audience, $parserToken->getPayload()->getAudience());
        $this->assertEquals($expiration, $parserToken->getPayload()->getExpiration());
        $this->assertEquals($notBefore, $parserToken->getPayload()->getNotBefore());
        $this->assertEquals($issuedAt, $parserToken->getPayload()->getIssuedAt());
        $this->assertEquals($uniqueId, $parserToken->getPayload()->getUniqueId());
    }
}
